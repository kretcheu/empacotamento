# Criando um ITP

## Vamos usar o reportbug para produzir o conteúdo do email.

## Passos iniciais

- instalar reportbug
- rodar para configurar

## Verificar se já não existe um BUG

```
wnpp-check pacote
```

## Criando o BUG ITP

```
reportbug -b
```

Selecione o pseudo pacote wnpp

Escolha (1) ITP

preencha os dados do pacote

Envie o email.



