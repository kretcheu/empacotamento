## Questionário para os alunos(as)

Qual seu nome?
Igor Garcia

Qual sua idade?
47

Qual sua área de formação? (não necessarimente acadêmica):
Ciência da Computação UFBA 1999

Qual sua área de atuação?
Engenheiro de Software

Há Quanto tempo usa GNU/Linux? 
- Debian-BR-CDD por alguns meses em 2005 (anexei uma pérola que achei nos meus arquivos de email!). Desisti por problemas de compatibilidade de hardware. 
- Ubuntu entre 2008 até 2021. 
- Debian desde 2021depois que conheci o Kretcheu na palestra na minidebconf 2020 e vi que o Debian tinha evoluído muito e aprendi mais também sobre Software verdadeiramente livre
- Nunca usei outra distro no meu desktop

Adminstra ou administrou servidores GNU/Linux?
Sim. Diversos servidores na AWS usando imagens Ubuntu server e CentOS, mas não uso muito avançado

Há quanto tempo usa Debian?
- Desde meados de 2021

Qual sua distribuição preferida?
- Debian

Em seu computador pessoal qual sistema e distribuição principal?
- Debian bookworm

Qual seu editor de texto puro preferido?
Vim. Mas pra programar, Emacs.

É programador(a)? Que linguagens?
- Sim. Clojure (LISP), Java, Python, Micropython, javascript, SQL...

Tem experiência em compilar programas?
- Sim, é minha atividade principal há mais de 20 anos.

Já criou pacotes deb?
- Não.

Já criou pacotes de alguma outra distribuição?
- Não.

