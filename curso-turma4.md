# Curso Empacotamento Debian (40h)
# 2022

## Objetivos

Ser capaz…
- de construir um pacote existente a partir dos fontes
- de fazer alterações em pacotes existentes
- de criar um pacote novo

Dinâmica:
- conceitos
- laboratórios
- atividades individuais
- oficinas
- material de apoio

## Conteúdo

- Fluxo dos pacotes no Debian
- Bugs RFP, ITP, RFS
- Anatomia de um pacote deb
- Métodos de empacotamento
- Preparação da jaula
- Pacotes fonte
- Diretório debian
- Ferramentas de desenvolvimento
- Construção de pacotes
- Atualização de pacotes
- Debianização
- Debian tracker
- Wnpp-check
- Debian policies
- Mais estudos (gbp, sbuild)

## Atividades

- Serão 8 aulas de 1h30 aproximadamente disponibilizadas aos alunos. (em duas versões)
- Laboratórios/tira-dúvidas ao vivo via jitsi em datas e horários combinadas com os participantes.
- Grupo de apoio e tira-dúvidas no Telegram.

## Material

- Serão disponibilizados os vídeos para participantes sob a licença livre CC-BY-SA 4.
- Material de apoio composto de slides, guias e links.

## Custo e condições

- R$ 200,00 via pix ou parcelado no cartão via pagseguro.

Pix:
kretcheu@gmail.com

PagSeguro
https://pag.ae/7XW9poQ13

## Contato

- e-mail: kretcheu@gmail.com
- Telegram: @kretcheu
