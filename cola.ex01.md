# Cola gbp

## Preparativos

### Instalando gbp
```
apt install git-buildpackage
```

### Alterando conf
/etc/git-buildpackage/gbp.conf

debian-branch = debian/master
pristine-tar = True

### Configurando git global
```
git config --global user.email "kretcheu@gmail.com"
git config --global user.name "Paulo Roberto Alves de Oliveira (aka kretcheu)"
```
--------------

## caso 1 (COM controle de versão)

(nethogs)
- baixar fontes
- empacotar

- clonar repo git
- importar dsc
- incluir remote
- push

## caso 2 (SEM controle de versão)

(epub2txt2)
- baixar fontes
- empacotar
- incluir VCs-*

- criar repo git
- importar dsc
- incluir remote
- push


