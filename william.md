# William

Qual seu nome?
R: William Verri Colioni

Qual sua idade?
R: Ribeirão Claro/PR

Qual sua área de formação? (não necessarimente acadêmica):
R: Ensino Superior Completo (Tecnologo em Segurança da Informação)

Qual sua área de atuação?
R: Trabalho em Provedor ISP Redgional

A Quanto tempo usa GNU/Linux?
R: 10 anos ±

Adminstra ou administrou servidores GNU/Linux?
R: sim

A quanto tempo usa Debian?
R: 4 anos

Qual sua distribuição preferida?
R: Debian e derivados

Em seu computador pessoal qual sistema e distribuição principal?
R: Linux Mint

Qual seu editor de texto puro preferido?
R: vim

É programador(a)? Que linguagens?
R: Não, mas gostou de trabalhar no bash e criar scripts

Tem experiência em compilar programas?
R: Pouca experiencia

Já criou pacotes deb?
R: Não

Já criou pacotes de alguma outra distribuição?
R: Não
