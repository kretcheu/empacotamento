# Curso Empacotamento Debian (30h)
# Janeiro 2022

## Objetivos

Ser capaz…
- de construir um pacote existente a partir dos fontes
- de fazer alterações em pacotes existentes
- de criar um pacote novo

Dinâmica:
- conceitos
- laboratórios
- atividades individuais
- oficinas
- material de apoio

## Conteúdo

- Fluxo dos pacotes no Debian
- Bugs RFP, ITP, RFS
- Anatomia de um pacote deb
- Métodos de empacotamento
- Preparação da jaula
- Pacotes fonte
- Diretório debian
- Ferramentas de desenvolvimento
- Construção de pacotes
- Atualização de pacotes
- Debianização
- Debian tracker
- Wnpp-check
- Debian policies
- Mais estudos (gbp, sbuild)

## Atividades

- Serão 8 aulas de 1h30 aproximadamente.
- 4 Laboratórios/tira-dúvidas ≃ 4h30min cada um.
- carga horária total ≃ 30h

## Material

- As aulas são ao vivo via Jitsi.
- Serão disponibilizados os vídeos para participantes.
- Material de apoio composto de slides, guias e links.

## Datas propostas

Data| Dia | Horário|     Aula |   Laboratório
:---:|:---:|:---:|:---:|:---:
5   | qua | 20h    | *** |
6   | qui | 20h    | *** |
8   | sáb | 14h    |     | ***
12  | qua | 20h    | *** |
13  | qui | 20h    | *** |
15  | sáb | 14h    |     | ***
19  | qua | 20h    | *** |
20  | qui | 20h    | *** |
22  | sáb | 14h    |     | ***
26  | qua | 20h    | *** |
27  | qui | 20h    | *** |
29  | sáb | 14h    |     | ***

-------
## calendário

Seg  | Ter | Qua | Qui | Sex | Sáb
:---:|:---:|:---:|:---:|:---:|:---:|
.    |  .   |  [5  |  [6] |  .  |  [[8]]
.    |  .   | [12] | [13] |  .  | [[15]]
.    |  .   | [19] | [20] |  .  | [[22]]
.    | .    | [26] | [27] |  .  | [[29]]

-------

```
[aula]
[[Laboratório]]
```

## Custo e condições

- Envie mensagem pessoal para @kretcheu no Telegram ou por e-mail: kretcheu@kretcheu.com.br

